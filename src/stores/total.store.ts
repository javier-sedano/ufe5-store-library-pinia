import { defineStore, createPinia, setActivePinia } from "pinia";
import { ref } from 'vue';
// @ts-ignore: TS6133: 'piniaPluginPersistedstate' is declared but its value is never read.
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

// Workaround from https://github.com/nuxt/nuxt/issues/22943#issuecomment-1702506306
const pinia = createPinia();
export default { store: setActivePinia(pinia) };

export const useTotalStore = defineStore('total', () => {
    const total = ref<number>(0);
    const at = ref<string>("?");

    function increment(amount: number): void {
        total.value = total.value + amount * 1;
        at.value = Date();
    }

    function reset(): void {
        total.value = 0;
        at.value = Date();
    }

    return { total, at, increment, reset };
}, {
    persist: true,
});
