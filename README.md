Part of the microfrontend reference architecture described in https://gitlab.com/javier-sedano/ufe5

Read that documentation.

This is a library of pinia stores to be used in the other projects of the architecture.
